# UFPI-MOBILE-ADMN

Esse é o **WebApp** que vai ser usado para gerenciar as informações que serão usados na **API** do **UFPI Mobile 2.0**.

## SETUP

Para poder usar essa aplicação em sua maquina, você tem que seguir os seguintes passos.

* Clonar o repositório.
> git clone 
* Instalar as depêndencias.
> npm install
* Iniciar o server.
> npm start
* Para iniciar o server no modo DEBUG e ter acesso ao log no console.
>DEBUG=ufpi-mobile-admin:* npm start